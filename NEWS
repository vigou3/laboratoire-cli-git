===
=== Ligne de commande et gestion de versions avec Git
===

# 2021.02 (2021-02-11)

## Nouveautés

- Introduction à Git: ajout d'images dans la diapositive «Vous avez
  vécu cette situation».

## Changements

- Branches Git: branche `release` dans l'exemple d'organisation avancé
  renommée `releases`.


# 2021.01 (2021-01-19)

## Nouveautés

- Ligne de commande: présentation de la commande `cp`.
- Ligne de commande: exercices sur la copie et la suppression de
  fichiers.
- Transfert de données et redirection: diapositive sur la philosophie
  Unix.
- Démarrer avec Git: ajout d'une mention qu'un fichier considéré
  «modifié» par Git peut soit avoir changé, soit avoir été ajouté sous
  suivi, soit avoir été supprimé. Ce dernier type de modification
  était mal compris.
- Bases de Git: ajout d'un «exercice» de préparatifs pour bien
  identifier, à la ligne de commande, le répertoire dans lequel la
  dépôt sera cloné afin de réduire une source fréquente de confusion.

## Changements

- Commandes essentielles: suppression de la présentation de la
  commande `touch`. Causait plus de confusion qu'autre chose.
- Commandes essentielles: section scindée en deux: «Navigation dans
  le système de fichiers» et «Gestion des fichiers».
- Diapositive «Rudiments de la ligne de commande» déplacée vers
  l'introduction.
- Diapositive sur les raccourcis clavier utiles déplacée vers
  l'introduction.


# 2020.08 (2020-08-31)

## Nouveautés

- Diapositive sur les raccourcis clavier utiles à la ligne de commande.
- Invite de commande colorée dans les exemples.


# 2020.02 (2020-02-12)

## Nouveautés

- Partie sur Git, section Aller plus loin: ajout d'une diapositive sur
  l'utilisation de Git dans RStudio.
  

# 2020.01 (2020-01-07)

## Nouveautés

- Représentation graphique du flux des données à la ligne de commande
  Unix.

## Changements

- Terme «archiver» utilisé comme équivalent français de «commit» tel
  que recommandé par l'Office québécois de la langue française.


# 2019.11 (2019-11-08)

## Nouveautés

- Diapositive sur quelques «Unix-ismes» importants pour utiliser la
  ligne de commande.
- Section sur l'exécution de scripts. Le matériel contient maintenant
  un petit fichier de script `bonjour.sh` pour les fins de l'exercice
  sur ce sujet.

## Corrections

- Dans l'explication de la commande de `git push -u`, l'argument
  `<branche>` est le nom de la branche à relier au serveur sur une
  branche du même nom, et non pas le nom de la branche sur le serveur
  à relier à la branche courante.
  

# 2019.10-3 (2019-10-07)

## Nouveautés

- Indication à l'effet qu'il faut quitter son éditeur pour terminer
  l'édition du message de validation de l'étape `git commit`.
- Avis de ne pas basculer vers une nouvelle branche sans avoir au
  préalable effectué au moins une publication dans `master`.
- Diapositive dans la section «Aller plus loin» (avec Git) indiquant
  où sont enregistrés les mots de passe de Git sous Windows et sous
  macOS. Cela devrait aider les personnes qui ont fait une erreur lors
  de l'entrée de leur mot de passe et qui souhaitent le réinitialiser.

## Changements

- Un des exercices sur la redirection et le transfert de données (une
  alternative de l'utilisation de `tr`) remplacé par la redirection de
  `ls` vers un fichier.
- Les noms fictifs d'arguments ont été changés de `<foo>` ou `<origin>`
  pour `<branche>` ou `<serveur>`, par exemple. Cette nomenclature est
  probablement plus facile à comprendre pour les novices.
- L'exercice sur les branches a été modifié légèrement pour faire
  ajouter un fichier dans la branche `foo` plutôt que simplement
  modifier un fichier existant. Cela permet de voir plus clairement
  encore qu'un fichier ajouté dans `foo` ne se trouve pas dans
  `master`. De plus, la dernière partie de l'exercice consiste
  maintenant à supprimer les nouvelles branches.
- Dans l'illustration de l'exemple d'organisation locale de
  «Programmer avec R», les serveurs sont maintenant identifiés par
  leurs étiquettes `origin` et `projets` plutôt que par les noms
  GitLab et BitBucket. Les icônes permettent toujours d'identifier le
  type de serveur utilisé.


# 2019.10-2 (2019-10-03)

## Changements

- La vidéo sur le travail collaboratif avec Git ayant changé, l'url a
  été modifiée pour mener vers la nouvelle version.


# 2019.10-1 (2019-10-01)

## Nouveautés

- Strip XKCD pour la partie sur la ligne de commande. Parce
  qu'autrement, ce n'était pas juste.
- Diapositive «Git vous parle, écoutez».

## Changements

- La configuration de `core.editor` sous macOS est `open -W -n` plutôt
  que simplement `open`.
- Uniformisation du vocabulaire: deux instances de «référentiel»
  remplacées par «dépôt».


# 2019.10 (2019-09-29)

Version initiale.