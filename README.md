<!-- Emacs: -*- coding: utf-8; eval: (auto-fill-mode -1); eval: (visual-line-mode t) -*- -->

# Ligne de commande et gestion de versions avec Git

> Ce projet est archivé depuis mai 2021 après avoir été scindé en deux projets autonomes: [*Ligne de commande Unix*](https://gitlab.com/vigou3/laboratoire-cli/) et [*Gestion de versions avec Git*](https://gitlab.com/vigou3/laboratoire-git/).

## Auteur

Vincent Goulet, professeur titulaire, École d'actuariat, Université Laval

## Licence

«Ligne de commande et gestion de versions avec Git» est mis à disposition sous licence
[Attribution-Partage dans les mêmes conditions 4.0
International](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)
de Creative Commons.

Consulter le fichier `LICENSE` pour la licence complète.
