%%% Copyright (C) 2021 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «Ligne de commande et gestion de versions avec Git»
%%% https://gitlab.com/vigou3/laboratoire-cli-git
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\part{Ligne de commande}

\begin{frame}[plain]
  \centering
  \begin{minipage}{0.8\linewidth}
    \includegraphics[width=\linewidth,keepaspectratio]{images/tar} \\
    \footnotesize Tiré de \href{https://xkcd.com/1168/}{XKCD.com}
  \end{minipage}
\end{frame}

\section{Introduction}

\begin{frame}
  \frametitle{Un outil puissant toujours pertinent}

  La ligne de commande du système d'exploitation demeure une interface
  importante pour les programmeurs.
  \begin{itemize}
  \item Parfois \alert{plus simple} qu'une interface graphique
  \item Souvent \alert{plus rapide} qu'une interface graphique
  \item Parfois la \alert{seule option} (notamment pour les
    utilitaires Unix comme \code{grep}, \code{sed}, \code{awk})
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Un peu de terminologie}

  \begin{description}
  \item[(Interface en) Ligne de commande] \mbox{} \\
    Mode d'interaction avec un programme informatique dans lequel
    l'utilisateur dicte les commandes et reçoit les réponses de
    l'ordinateur en mode texte (\emph{command line interface}, CLI)
  \item[Interpréteur de commandes] \mbox{} \\
    Programme qui gère l'interface en ligne de commande
    (\emph{terminal}, \emph{shell})
  \item[Invite de commande] \mbox{} \\
    Symbole affiché par l'interpréteur de commande pour indiquer qu'il
    est prêt à recevoir une commande (\emph{command prompt})
  \end{description}
\end{frame}

\begin{frame}
  \frametitle{Ligne de commande Windows}

  \begin{minipage}[t]{0.45\linewidth}
    \begin{block}{\texttt{cmd.exe}}
      \vskip1em
      \begin{itemize}
      \item Accessoires | Invite de commandes
      \item pas très puissant
      \item invite par défaut: \code{\color{prompt}C:\string\>}
      \end{itemize}
      \includegraphics[width=\linewidth,keepaspectratio]{images/cmd}
    \end{block}
  \end{minipage}
  \hfill
  \begin{minipage}[t]{0.45\linewidth}
    \begin{block}{\emph{Git Bash} ou \texttt{MSYS}}
      \vskip1em
      \begin{itemize}
      \item interpréteur Bash très puissant
      \item standard Unix
      \item invite par défaut: \code{\color{prompt}\$}
      \end{itemize}
      \includegraphics[width=\linewidth,keepaspectratio]{images/git-bash}
    \end{block}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Ligne de commande macOS}

  \begin{block}{Terminal}
    \begin{itemize}
    \item Applications | Utilitaires | Terminal ou via Spotlight
    \item interpréteur Bash ou Z Shell (à partir de macOS 10.15
      Catalina)
    \item invite par défaut: \code{\color{prompt}\$}
    \end{itemize}

    \begin{minipage}{0.45\linewidth}
      \includegraphics[width=\linewidth,keepaspectratio]{images/terminal}
    \end{minipage}
    \hfill
    \begin{minipage}{0.52\linewidth}
      \tipbox{\small Vous pouvez modifier l'apparence de la
        ligne de commande dans les préférences de l'application
        Terminal}
    \end{minipage}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Rudiments de la ligne de commande}

  Nous allons uniquement étudier les commandes qui permettent de:
  \begin{itemize}
  \item se déplacer dans le système de fichiers
  \item afficher la liste des fichiers d'un répertoire
  \item afficher le contenu d'un fichier
  \item copier et supprimer un fichier
  \end{itemize}

  \warningbox{Commandes Bash (Unix) seulement.}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Quelques Unix-ismes}

  \begin{itemize}
  \item Répertoire par défaut est normalement le \alert{répertoire
      personnel} (\emph{home directory}) \\
    \medskip
    \begin{minipage}{0.45\linewidth}
      \small Windows
      \vspace{-1ex}
\begin{lstlisting}
---$- printenv HOME
/c/Users/vincent
\end{lstlisting}
    \end{minipage}
    \hfill
    \begin{minipage}{0.45\linewidth}
      \small macOS
      \vspace{-1ex}
\begin{lstlisting}
---$- printenv HOME
/Users/vincent
\end{lstlisting}
    \end{minipage}
  \item Répertoire personnel identifié par le symbole
    «\code{\string~}»
  \item Noms de répertoires séparés par la barre oblique «\code{/}»
  \item Ligne de commande sensible à la casse (\code{foo} $\neq$
    \code{Foo} $\neq$ \code{FOO})
  \item Fichiers \code{.foo} cachés dans la liste des fichiers
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Quelques raccourcis clavier utiles}

  \tipbox{Gagnez en efficacité au clavier!
    \begin{ttscript}{Ctrl-M}
    \item[$\uparrow$ | $\downarrow$] commande
      précédente~|~suivante dans l'historique
    \item[\code{\tabkey}] autocomplétion de la commande, du nom de
      fichier, etc. \newline
      \alert{votre meilleure alliée!}
    \item[\texttt{Ctrl-R}] recherche dans l'historique des commandes
    \item[\texttt{Ctrl-K}] suppression du texte qui suit le curseur
    \item[\texttt{Ctrl-U}] suppression du texte qui précède le curseur
    \end{ttscript}
    Il y en a
    \link{https://en.wikipedia.org/wiki/GNU_Readline}{plusieurs autres}.%
  }
\end{frame}

\begin{frame}
  \frametitle{\faCogs\; Exercice}

  \begin{enumerate}
  \item Démarrer la ligne de commande de votre système d'exploitation
  \item Identifier le répertoire courant dans l'invite de commande
  \item Repérer ce répertoire dans le système de fichier avec
    l'Explorateur Windows ou le Finder (garder la fenêtre ouverte)
  \end{enumerate}
\end{frame}


\section[Navigation dans le système \\ de fichiers]{Navigation dans le système de fichiers}

\begin{frame}[fragile=singleslide]
  \frametitle{Afficher le répertoire courant}

  \code{pwd} \quad (\emph{print working directory})
  \begin{itemize}
  \item affiche le chemin d'accès absolu du répertoire courant

\begin{lstlisting}
---~$- pwd
/Users/vincent
\end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Changer de répertoire}

  \code{cd} \quad (\emph{change directory})
  \begin{itemize}
  \item change le répertoire courant pour
    celui donné en argument
  \item argument peut être un chemin d'accès \alert{absolu} ou
    \alert{relatif}
  \item nom fictif «\code{.}» identifie le répertoire courant
  \item nom fictif «\code{..}» identifie le parent du répertoire
    courant
  \item sans argument, ramène au répertoire personnel «\code{\string~}»

\begin{lstlisting}
---~$- cd Desktop/
/Users/vincent/Desktop
---~/Desktop$- cd ~/Documents/cours/
---~/Documents/cours$- cd
---~$-
\end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Lister les fichiers}

  \code{ls} \quad (\emph{list})
  \begin{itemize}
  \item affiche les fichiers du répertoire en argument
  \item sans argument, affiche les fichiers du répertoire courant

\begin{lstlisting}
---~$- ls
Desktop
Documents
Downloads
<...>
\end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{\faCogs\; Exercice}

  \small
  \begin{enumerate}
  \item Afficher le répertoire courant. Comparer avec celui mentionné
    dans l'invite de commande.
  \item Afficher la liste des fichiers du répertoire courant. Comparer
    avec la liste de l'Explorateur Windows ou du Finder.
  \item Choisir un sous-répertoire du répertoire courant que vous
    savez contenir lui-même un sous-répertoire (par exemple:
    \code{Documents/Cours}).
  \item Afficher, \alert{sans d'abord s'y déplacer}, la liste des
    fichiers du premier sous-répertoire (\code{Documents}), puis celle
    du second (\code{Cours}).
  \item Faire du sous-sous-répertoire ci-dessus (\code{Cours}) le
    répertoire courant.
  \item Afficher une liste détaillée des fichiers du nouveau
    répertoire courant avec la commande \code{ls~-l}.
  \item Revenir au répertoire personnel avec une seule commande.
  \item Afficher la liste de tous les fichiers du répertoire
    personnel, y compris les fichiers cachés, avec la commande
    \code{ls~-a}.
  \end{enumerate}
\end{frame}


\section{Gestion des fichiers}

\begin{frame}[fragile=singleslide]
  \frametitle{Afficher le contenu d'un fichier}

  \code{cat} \quad (\emph{catenate})
  \begin{itemize}
  \item affiche le contenu du fichier donné en argument
  \item utile uniquement pour les fichiers en texte brut

\begin{lstlisting}
---$- cat hello.txt
Hello World!
\end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Copier un fichier}

  \code{cp} \quad (\emph{copy})
  \begin{itemize}
  \item copie le fichier en premier argument vers la destination en
    second argument
  \item second argument peut être un nom de répertoire, un nom de
    fichier ou les deux

\begin{lstlisting}
---$- cp hello.txt foo.txt
---$- cp hello.txt Documents/
---$- cp hello.txt Documents/foo.txt
\end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Supprimer un fichier}

  \code{rm} \quad (\emph{remove})
  \begin{itemize}
  \item supprime le ou les fichiers en argument
  \item fichiers disparus à jamais

\begin{lstlisting}
---$- rm foo
\end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{\faCogs\; Exercice}

  Faire de votre répertoire personnel le répertoire courant et créer
  un fichier (vide) nommé \code{foobar} avec la commande:
\begin{lstlisting}
---$- touch foobar
\end{lstlisting}

  Effectuer les opérations ci-dessous en validant chaque fois leur
  effet dans l'Explorateur Windows ou dans le Finder.
  \begin{enumerate}
  \item Copier le fichier \code{foobar} dans votre répertoire
    personnel sous le nom \code{foo.txt}.
  \item Copier le fichier \code{foobar} dans le répertoire
    \code{\string~/Documents}.
  \item Copier le fichier \code{foobar} dans le répertoire
    \code{\string~/Documents} sous le nom \code{bar.txt}.
  \item Copier le fichier \code{\string~/Documents/bar.txt} dans le
    répertoire courant. (\emph{Astuce}: utiliser un nom de répertoire
    fictif.)
  \item Supprimer tous les fichiers créés ci-dessus.
  \end{enumerate}
\end{frame}

\section[Transfert de données \\ et redirection]{Transfert de données et redirection}

\begin{frame}
  \frametitle{Philosophie Unix}

  \alert{«Ne faire qu'une seule chose, et la faire bien.»}

  \begin{itemize}
  \item Écrivez des programmes qui effectuent une seule chose et qui
    le font bien.
  \item Écrivez des programmes qui collaborent.
  \item Écrivez des programmes pour gérer des flux de texte, car c'est
    une interface universelle.
  \end{itemize}

  {\small (Source: Wikipedia)}
\end{frame}

\begin{frame}
  \frametitle{Entrée et sortie standard}

  Les programmes Unix en ligne de commande:
  \begin{itemize}
  \item reçoivent leurs données d'une \alert{entrée standard}
    (\emph{standard input}, stdin)
  \item écrivent leurs résultats vers la \alert{sortie standard}
    (\emph{standard output}, stdout)
  \item émettent leurs erreurs vers l'\alert{erreur standard}
    (\emph{standard error}, stderr)
  \end{itemize}

  \tipbox{Dans un terminal, l'entrée standard est le clavier et la
    sortie et l'erreur standards, l'écran}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Transfert de données}

  \emph{stdout} \code{\textbar} \emph{stdin} \quad («tuyau», \emph{pipe})

  \begin{itemize}
  \item transfère la sortie d'un programme directement à l'entrée d'un
    autre programme
  \item premier programme passe son résultat en entrée au second
    programme
  \item similaire à la composition de fonctions $g \circ f$ en mathématiques

\begin{lstlisting}
---~$- pwd
/Users/vincent
---~$- pwd | cut -d / -f 3
vincent
\end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Redirection}

  \emph{stdout} \code{>} \emph{fichier}

  \begin{itemize}
  \item redirige la sortie standard d'un programme vers un fichier
  \item si le fichier existe déjà, son contenu est écrasé
  \item variante double \code{>>} pour ajouter le contenu à la fin du
    fichier
\begin{lstlisting}
---$- echo 'Hello World!' > hello.txt
---$- cat hello.txt
Hello World!
---$- echo 'Salut la compagnie!' >> hello.txt
---$- cat hello.txt
Hello World!
Salut la compagnie!
\end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Redirection (suite)}

  \emph{stdin} \code{<} \emph{fichier}

  \begin{itemize}
  \item déverse le contenu d'un fichier dans l'entrée standard d'un
    programme
  \item variante double existe aussi

\begin{lstlisting}
---$- tr ! ? < hello.txt
Hello World?
Salut la compagnie?
\end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Représentation graphique}

  \centering
  \setlength{\unitlength}{4mm}
  \small
  \begin{picture}(31.25,9)(-0.25,-1.5)
    \thicklines
    \put(3,6){\makebox(0,0)[t]{\parbox{6.5\unitlength}{
          \centering{\LARGE\faStream} \\[4pt]
          entrée standard}}}
    \put(6,5){\vector(1,0){3.5}}
    \put(12,6){\makebox(0,0)[t]{\parbox{6\unitlength}{
          \centering{\LARGE\faCogs} \\[4pt]
          commande}}}
    \put(14.75,5){\line(1,0){1.5}}
    \put(16.25,7){\line(0,-1){4}}
    \put(16.25,7){\vector(1,0){1.75}}
    \put(16.25,3){\vector(1,0){1.75}}
    \put(21,8){\makebox(0,0)[t]{\parbox{6.5\unitlength}{
          \centering{\LARGE\faStream} \\[4pt]
          erreur standard}}}
    \put(21,4){\makebox(0,0)[t]{\parbox{6\unitlength}{
          \centering{\LARGE\faStream} \\[4pt]
          sortie standard}}}
    \put(23.75,3){\vector(1,0){2.75}}
    \put(25,3.5){\makebox(0,0){\code{>}}}
    \put(28,4){\makebox(0,0)[t]{\parbox{4\unitlength}{
          \centering{\LARGE\faFile*[regular]} \\[4pt]
            fichier}}}
    \Line(21,0.75)(21,0)(3,0)
    \put(3,0){\vector(0,1){2.5}}
    \put(16.25,0.5){\makebox(0,0){\code{\textbar}}}
    \Line(28,0.75)(28,-1)(12,-1)(12,0)
    \put(25,-0.5){\makebox(0,0){\code{<}}}
  \end{picture}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{\faCogs\; Exercice}

  Créer un fichier nommé \code{foobar} depuis la ligne de commande
  avec la commande:
\begin{lstlisting}
---$- echo 'aaa.bbb.ccc' > foobar
\end{lstlisting}

  Déterminer ensuite le résultat des commandes suivantes.
  \begin{enumerate}
  \item \code{cat foobar}
  \item \code{cat foobar | cut -d . -f 1}
  \item \code{tr a z < foobar}
  \item \code{ls -la \string~ > foobar} (examiner le contenu du fichier avec \code{cat})
  \item \code{rm foobar}
  \end{enumerate}
\end{frame}


\section{Exécution de scripts}

\begin{frame}[fragile=singleslide]
  \frametitle{Scripts Bash}

  Bash contient un langage de programmation afin de créer des scripts
  pour automatiser des tâches.

  \begin{itemize}
  \item Simples fichiers texte avec sur la première ligne
    \medskip
    \begin{minipage}{0.45\linewidth}
\begin{lstlisting}
#!/bin/sh
\end{lstlisting}
    \end{minipage}
    \hfill ou \hfill
    \begin{minipage}{0.45\linewidth}
\begin{lstlisting}
#!/bin/bash
\end{lstlisting}
    \end{minipage}
  \item
    \link{https://fr.wikipedia.org/wiki/Permissions_UNIX}{Permissions
      Unix} du fichier doivent inclure le \alert{droit d'exécution}
\begin{lstlisting}
---$- ls -l bonjour.sh
-rw`\textcolor{alert}{x}'r--r--  1 vincent  staff  136  8 nov 09:18 bonjour.sh
\end{lstlisting}
  \item Ajouter le droit d'exécution (pour l'utilisateur seulement)
\begin{lstlisting}
---$- chmod u+x bonjour.sh
\end{lstlisting}
  \item Exécuter le script
\begin{lstlisting}
---$- ./bonjour.sh
\end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{\faCogs\; Exercice}

  Le fichier \code{bonjour.sh} est livré avec cette formation.

  \begin{enumerate}
  \item Faire du répertoire contenant le matériel de la formation le
    répertoire courant.
  \item Afficher le contenu du fichier \code{bonjour.sh}.
  \item Vérifier les permissions du fichier avec la commande \code{ls
      -l}.
  \item Si nécessaire, ajouter le droit d'exécution au fichier et
    répéter l'étape précédente.
  \item Exécuter le script \code{bonjour.sh}.
  \end{enumerate}
\end{frame}

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: "laboratoire-cli-git"
%%% coding: utf-8
%%% End:
